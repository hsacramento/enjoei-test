class Message < ActiveRecord::Base
  scope :recent, -> { order("messages.created_at DESC") }

  validates :title, :content, presence: true

  state_machine :state, initial: :unread do

    event :read do
      transition unread: :read
    end

    event :archive do
      transition any => :archived
    end

    before_transition to: :read do |message|
      message.read_at = Time.current
    end

    before_transition to: :archived do |message|
      message.archived_at = Time.current
    end
  end
end

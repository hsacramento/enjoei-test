App.messages.index = App.messages.index || {}

class App.messages.index.archiveAllMessages

  constructor: (@options = {}) ->
    @make()
    @bind()

  make: () ->
    @elementClass = '.js-archive-all-messages'
    @targetsClass = '.message-row'
    @$element = $(@elementClass)
    @$targets = $(@targetsClass)

  bind: () ->
    @$element.on 'click', (event) =>
      @rows = @$targets
      event.preventDefault()
      NProgress.start()
      $.ajax({
        url: @$element.attr('href'),
        type: 'post'
      }).always( =>
        @rows.hide()
        NProgress.done()
      )

